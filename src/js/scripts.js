// To do
// [x] Search functionality, highlighting the results
// [x] Top image slider, randomize slider image on page refresh/load
// [x] Add a read more button for articles

window.onload = function () {
    // Slide randomizer
    var totalSlides = $('.carousel-item').length;
    var randomizeSlide = Math.floor((Math.random() * totalSlides));
    $('.carousel-inner').find('.carousel-item').eq(randomizeSlide).addClass('active');

    $(function () {
        var $context = $(".body");
        var $form = $("form");
        var $button = $form.find("button[name='perform']");
        var $input = $form.find("input[name='keyword']");

        $button.on("click.perform", function () {

            // Determine search term
            var searchTerm = $input.val();
            // Remove old highlights and highlight
            // new search term afterwards
            $context.removeHighlight();
            $context.highlight(searchTerm);

        });
        $button.trigger("click.perform");
    });
};

$(document).ready(function () {
    // Dynamic read more functionality
    // targeted text lenght in chars 
    var charLimit = 208;
    // add ellipses cutoff
    var ellipses = "..."
    // button texts
    var moreText = "Read more";
    var lessText = "Read less";

    $('.excerpt').each(function () {
        var content = $(".excerpt p").html();

        if (content.length > charLimit) {
            // split string into two
            var textShown = content.substr(0, charLimit);
            var textCut = content.substr(charLimit, content.length - charLimit);

            // prepare output 
            var output = textShown +'<span class="ellipses">' + ellipses + '</span><span class="cut-content"><span>' + textCut + '</span><a href="" class="btn btn-primary read-more-link">' + moreText + '</a></span>';
            // output html
            $(this).html(output);
        }

        // show and hide functionality
        $(".read-more-link").on("click", function(e) {
            if ($(this).hasClass('read-less')) {
                $(this).removeClass('read-less');
                $(this).html(moreText);
            } else {
                $(this).addClass("read-less");
                $(this).html(lessText);
            }
            // toggle display
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            e.preventDefault();
            e.stopPropegation(); 
        })
        
    });
    // mark.js settings
    function highlight(param) {
  
        // Select the whole paragraph
        var ob = new Mark(document.querySelector(".body"));
        // First unmark the highlighted word or letter
        ob.unmark();
        // Highlight letter or word
        ob.mark(
            document.getElementById("search").value,
            { element: 'span',
              className: 'highlight',   
            }
        );
    }
    // Searchbox trigger on keydown enter or click on btn
    $('#search').keydown(function(e){
        if(e.key === 'Enter') {
            highlight($(this).val());
        }
    }); 
    
});